"use strict";

let nav = document.querySelector('nav');

const mobileBtn = document.getElementById('mobile-cta');
mobileBtn.addEventListener('click', () => {
    nav.classList.add('menu-btn');
})

const mobileBtnExit = document.getElementById('mobile-exit');
mobileBtnExit.addEventListener('click', () => {
    nav.classList.remove('menu-btn');
})

// function showMenu() {
//     let nav = document.querySelector('nav');
//     nav.classList.add('menu-btn');
// }

// function hideMenu() {
//     // let nav = document.querySelector('nav');
//     nav.classList.remove('menu-btn');
// }

